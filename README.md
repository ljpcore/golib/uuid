![](icon.png)

# uuid

A simple module used to generate Version 4, Variant 1 UUIDs as per RFC 4122.

## Usage Example

```go
userID := uuid.New()
fmt.Printf("%v\n", userID) // 2c99fffe-56b8-4453-9f6a-32a8d7fbde80
```

## Support

The `uuid.UUID` type supports marshalling and unmarshalling from JSON and works
with most standard UUID/GUID database types.

---

Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from
[www.flaticon.com](https://www.flaticon.com/).
