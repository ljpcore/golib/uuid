package uuid

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/does"
	"gitlab.com/ljpcore/golib/test/has"
	"gitlab.com/ljpcore/golib/test/is"
)

func TestNewDoesNotPanic(t *testing.T) {
	// Arrange, Act and Assert.
	test.That(t, func() { New() }, does.NotPanic)
}

func TestNewIsNotEmpty(t *testing.T) {
	// Arrange and Act.
	id := New()

	// Assert.
	test.That(t, id, is.NotEqualTo(Empty))
}

func TestUUIDStringIncludesHashes(t *testing.T) {
	// Arrange.
	id := New()

	// Act.
	str := id.String()

	// Assert.
	test.That(t, str, has.Length(StringSize))
}

func TestUUIDStringHasExpectedVersionAndVariant(t *testing.T) {
	f := func() {
		// Arrange.
		id := New()

		// Act.
		str := id.String()

		// Assert.
		m := str[14]
		n := str[19]
		np := n == '8' || n == '9' || n == 'a' || n == 'b'

		test.That(t, m, is.EqualTo(uint8('4')))
		test.That(t, np, is.True)
	}

	for i := 0; i < 1000; i++ {
		f()
	}
}

func TestNewFromErrorForBadRandomSource(t *testing.T) {
	// Arrange and Act.
	id, err := NewFrom(&alwaysFailsReader{})

	// Assert.
	test.That(t, err, is.NotNil)
	test.That(t, err.Error(), is.EqualTo("cannot generate UUID from provided source: always fails to read"))
	test.That(t, id, is.EqualTo(Empty))
}

func TestParseSuccess(t *testing.T) {
	// Arrange.
	str := "2c99fffe-56b8-4453-9f6a-32a8d7fbde80"

	// Act.
	id, err := Parse(str)

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, id.String(), is.EqualTo(str))
}

func TestParseInvalidHex(t *testing.T) {
	// Arrange.
	str := "Hc99fffe-56b8-4453-9f6a-32a8d7fbde80"

	// Act.
	id, err := Parse(str)

	// Assert.
	test.That(t, err, is.NotNil)
	test.That(t, err.Error(), is.EqualTo("failed to parse UUID: encoding/hex: invalid byte: U+0048 'H'"))
	test.That(t, id, is.EqualTo(Empty))
}

func TestParseInvalidLength(t *testing.T) {
	// Arrange.
	str := "99fffe-56b8-4453-9f6a-32a8d7fbde80"

	// Act.
	id, err := Parse(str)

	// Assert.
	test.That(t, err, is.NotNil)
	test.That(t, err.Error(), is.EqualTo("the provided UUID was not 16 bytes in length"))
	test.That(t, id, is.EqualTo(Empty))
}

func TestUUIDMarshalJSON(t *testing.T) {
	// Arrange.
	id := New()
	str := id.String()

	// Act.
	json, err := id.MarshalJSON()

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, string(json), is.EqualTo(fmt.Sprintf(`"%v"`, str)))
}

func TestMarshalAndUnmarshalJSONSuccess(t *testing.T) {
	// Arrange.
	c1 := &uuidContainer{
		ContainerID: New(),
	}

	// Act.
	raw, err := json.Marshal(c1)
	test.That(t, err, is.Nil)

	c2 := &uuidContainer{}
	err = json.Unmarshal(raw, c2)

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, c2.ContainerID, is.EqualTo(c1.ContainerID))
}

func TestUUIDUnmarshalJSONInvalidSize(t *testing.T) {
	// Arrange.
	id := &UUID{}
	raw := []byte(`"2c99fffe-56b8-4453-9f6a-32a8d7fbde"`)

	// Act.
	err := id.UnmarshalJSON(raw)

	// Assert.
	test.That(t, err, is.NotNil)
	test.That(t, err.Error(), is.EqualTo("JSON unmarshal of UUID failed: the provided UUID was not 16 bytes in length"))
}

func TestUUIDUnmarshalJSONInvalidHex(t *testing.T) {
	// Arrange.
	id := &UUID{}
	raw := []byte(`"2c99fffe-56b8-4453-9f6a-32a8d7fbdeGG"`)

	// Act.
	err := id.UnmarshalJSON(raw)

	// Assert.
	test.That(t, err, is.NotNil)
	test.That(t, err.Error(), is.EqualTo("JSON unmarshal of UUID failed: failed to parse UUID: encoding/hex: invalid byte: U+0047 'G'"))
}

func TestUUIDValue(t *testing.T) {
	// Arrange.
	id := New()

	// Act.
	val, err := id.Value()

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, val, is.NotNil)
	test.That(t, val.(string), is.EqualTo(id.String()))
}

func TestUUIDScanNil(t *testing.T) {
	// Arrange.
	id := New()

	// Pre-condition.
	test.That(t, id, is.NotEqualTo(Empty))

	// Act.
	err := id.Scan(nil)

	// Assert.
	test.That(t, err, is.Nil)
	test.That(t, id, is.EqualTo(Empty))
}

func TestUUIDScanNotString(t *testing.T) {
	// Arrange.
	id := &UUID{}

	// Act.
	err := id.Scan(5)

	// Assert.
	test.That(t, err, is.NotNil)
	test.That(t, err.Error(), is.EqualTo("expected to scan a string to UUID, but was '5'"))
}

func TestUUIDScanParseFailure(t *testing.T) {
	// Arrange.
	id := &UUID{}

	// Act.
	err := id.Scan("2c99fffe-56b8-4453-9f6a-32a8d7fbdeGG")

	// Assert.
	test.That(t, err, is.NotNil)
	test.That(t, err.Error(), is.EqualTo("database scan of UUID failed: failed to parse UUID: encoding/hex: invalid byte: U+0047 'G'"))
}

func TestUUIDScanSuccess(t *testing.T) {
	// Arrange.
	id := &UUID{}

	// Act.
	err := id.Scan("2c99fffe-56b8-4453-9f6a-32a8d7fbdedd")

	// Assert.
	test.That(t, err, is.Nil)
}

// --

type alwaysFailsReader struct{}

var _ io.Reader = &alwaysFailsReader{}

func (*alwaysFailsReader) Read(b []byte) (int, error) {
	return 0, errors.New("always fails to read")
}

type uuidContainer struct {
	ContainerID UUID
}
