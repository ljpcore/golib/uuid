package uuid

import (
	"crypto/rand"
	"database/sql"
	"database/sql/driver"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"strings"
)

// UUID stores a single 128-bit UUID.
type UUID [Size]byte

var _ fmt.Stringer = UUID{}
var _ json.Marshaler = UUID{}
var _ json.Unmarshaler = &UUID{}
var _ driver.Valuer = UUID{}
var _ sql.Scanner = &UUID{}

// Size is the size, in bytes, of a UUID.
const Size = 16

// StringSize is the size, in bytes, of a UUID string with hyphens.
const StringSize = 36

// Empty returns the empty UUID - that is, a UUID with all 128-bits as zero.
// Contrary to popular belief, the Empty UUID can never be generated, even under
// Version 4 UUIDs.
var Empty = UUID{}

// ErrInvalidSize is returned when parsing a UUID that is not 16 bytes in
// length.
var ErrInvalidSize = errors.New("the provided UUID was not 16 bytes in length")

// New creates a new Version 4, Variant 1 UUID using the standard library
// CSPRNG.  It is assumed that this CSPRNG is always available to ease error
// checking when generating UUIDs.  If random bytes cannot be read, this call
// will panic.
func New() UUID {
	id, err := NewFrom(rand.Reader)
	if err != nil {
		panic(err)
	}

	return id
}

// NewFrom creates a new Version 4, Variant 1 UUID using the provided source of
// random numbers.
func NewFrom(r io.Reader) (UUID, error) {
	id := UUID{}

	_, err := io.ReadFull(r, id[:])
	if err != nil {
		return Empty, fmt.Errorf("cannot generate UUID from provided source: %w", err)
	}

	// Clear first 4 bits of the 7th byte.
	id[6] <<= 4
	id[6] >>= 4

	// Set the 2nd bit of the 7th byte to 1 (for Version 4).
	id[6] |= 1 << 6

	// Clear the first 2 bits of the 10th byte.
	id[8] <<= 2
	id[8] >>= 2

	// Set the most-significant bit of the 10th byte to 1 (for Variant 1).
	id[8] |= 1 << 7

	return id, nil
}

// Parse parses the provided UUID string to a UUID structure.  Parse allows
// UUIDs with different versions and variants.
func Parse(str string) (UUID, error) {
	str = strings.TrimSpace(strings.ReplaceAll(str, "-", ""))

	raw, err := hex.DecodeString(str)
	if err != nil {
		return Empty, fmt.Errorf("failed to parse UUID: %w", err)
	}

	if len(raw) != Size {
		return Empty, ErrInvalidSize
	}

	id := UUID{}
	copy(id[:], raw)

	return id, nil
}

// String converts the UUID to the standard UUID string representation with
// hyphens.
func (id UUID) String() string {
	str := hex.EncodeToString(id[:])

	sb := strings.Builder{}
	sb.WriteString(str[:8])
	sb.WriteRune('-')
	sb.WriteString(str[8:12])
	sb.WriteRune('-')
	sb.WriteString(str[12:16])
	sb.WriteRune('-')
	sb.WriteString(str[16:20])
	sb.WriteRune('-')
	sb.WriteString(str[20:32])

	return sb.String()
}

// MarshalJSON marshals the UUID into a JSON string literal.
func (id UUID) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`"%v"`, id.String())), nil
}

// UnmarshalJSON unmarshals the provided JSON literal into the UUID.
func (id *UUID) UnmarshalJSON(raw []byte) error {
	str := string(raw)
	if len(str) != StringSize+2 {
		return fmt.Errorf("JSON unmarshal of UUID failed: %w", ErrInvalidSize)
	}

	str = str[1:37]
	tID, err := Parse(str)
	if err != nil {
		return fmt.Errorf("JSON unmarshal of UUID failed: %w", err)
	}

	*id = tID
	return nil
}

// Value returns a database-driver-friendly representation of the UUID.
func (id UUID) Value() (driver.Value, error) {
	return id.String(), nil
}

// Scan implements the logic used to read a database value.
func (id *UUID) Scan(src interface{}) error {
	if src == nil {
		*id = Empty
		return nil
	}

	str, ok := src.(string)
	if !ok {
		return fmt.Errorf("expected to scan a string to UUID, but was '%v'", src)
	}

	tID, err := Parse(str)
	if err != nil {
		return fmt.Errorf("database scan of UUID failed: %w", err)
	}

	*id = tID
	return nil
}
