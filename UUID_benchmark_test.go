package uuid

import (
	"encoding/json"
	"testing"

	"gitlab.com/ljpcore/golib/test"
	"gitlab.com/ljpcore/golib/test/is"
)

func BenchmarkUUIDStringParse(b *testing.B) {
	for n := 0; n < b.N; n++ {
		str := New().String()
		id, err := Parse(str)
		test.That(b, err, is.Nil)
		test.That(b, id, is.NotEqualTo(Empty))
	}
}

func BenchmarkUUIDJSON(b *testing.B) {
	for n := 0; n < b.N; n++ {
		id1 := New()

		raw, err := json.Marshal(id1)
		test.That(b, err, is.Nil)

		id2 := &UUID{}
		err = json.Unmarshal(raw, id2)
		test.That(b, err, is.Nil)
	}
}
